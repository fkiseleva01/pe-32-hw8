
const div = document.createElement("div")
div.style.width = '300px'
div.style.height = '100px'
div.style.marginTop = "10px"
div.style.backgroundColor = '#E9967A'
div.style.display = "flex"
document.body.append(div)

const p = document.createElement("p")
p.innerText = "Prise"
p.className = "text-price"
p.style.color = "white"
p.style.fontFamily = " sans-serif"
p.style.paddingLeft = "30px"
p.style.fontSize = "30px"
div.appendChild(p)

const input = document.createElement("input")
input.type = "number"
input.id = "input-box"
input.style.marginLeft = "20px"
input.style.height = "30px"
input.style.marginTop = "30px"
div.appendChild(input)

const spanValue = document.createElement("span")
spanValue.id = "spanValue"
spanValue.style.fontFamily = "sans-serif"
div.insertAdjacentElement("beforebegin", spanValue)

const btnClose = document.createElement("button")
btnClose.textContent = "x"
btnClose.style.backgroundColor = "#30d5c8"
btnClose.style.marginLeft = "10px"
btnClose.style.visibility = "hidden"
spanValue.insertAdjacentElement("afterend", btnClose)

const spanError = document.createElement("span")
spanError.id = "error"
spanError.style.fontFamily = "sans-serif"
div.insertAdjacentElement("afterend", spanError)




btnClose.addEventListener("click", () => {
    btnClose.remove()
    spanValue.remove()
    input.value = ""
    input.style.backgroundColor = "#fff"
    spanError.style.visibility = "hidden"
})


input.addEventListener("blur", ()=>{
    if (input.value > 0){
        const spanValue = document.getElementById("spanValue")
        spanValue.innerHTML = `Текущая цена: ${input.value}`
        input.style.backgroundColor = "#30d5c8"
        btnClose.style.visibility = "visible"
        input.style.border = ""

    } else if (input.value < 0 ) {

        const spanError = document.getElementById("error")
        spanError.style.visibility = "visible"
        spanError.innerHTML = "Please enter correct price"
        input.style.border = "2px solid red"
       
    }
    
})

input.addEventListener("focus", ()=>{
    input.style.backgroundColor = "#fff"
    spanError.style.visibility= "hidden"
})





